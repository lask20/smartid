var fs = require('fs');
var request = require('request');
let config = require("./config.js");

class SmartIDDevice{

	constructor(mqtt){
		this._mqtt = mqtt;
		this._model = "";
		this._connected = false;
		this.id = "";
		this._version = null;
		this._token = null;
		this._userid = null;
		this._fingerType = null
	}

	isConnected() {
		return this._connected;
	}

	getModel() {
		return this._model;
	}

	getVersion() {
		return this._version;
	}

	_onMessageMqtt(topic, message) {
		var topic_s = topic.split("/");
		if (topic_s[0] === this.id && topic_s[1] === "send") {
			if (topic_s[2] === "fp") {
				this._writeToFile(topic_s[3],message);
			}
		}
	}

	start() {
		this._mqtt.subscribe(this.id+"/send/#");
		this._mqtt.on('message', this._onMessageMqtt.bind(this));
		this._log(`started id #${this.id}`);
	}

	_command(data) {
		this._mqtt.publish(this.id+"/recv", JSON.stringify(data));
	}

	enroll(user_id,finger,name,token) {
		this._token = token;
		this._fingerType = finger;
		this._userid = user_id;
		this._command({type:"enroll",user_id:user_id,name:name,finger:finger});
	}

	removeFinger(finger_id) {
		this._command({type:"remove_figer",id:finger_id});
	}

	removeFingerAll() {
		this._command({type:"remove_figer_all"});
	}

	updateTime() {
		var date = new Date();
		this._command({type:"update_datetime",hour:date.getHours(),min:date.getMinutes(),sec:date.getSeconds(),day:date.getDate(),month:date.getMonth()+1,year:date.getFullYear()});
	}

	//{"type":"upload_screen","host":"192.168.130.176","port":80,"file":"/smart.bin"}
	uploadScreen(host,port,file) {
		this._command({type:"upload_screen",host:host,port:port,file:file});
	}

	_writeToFile(name,data) {
		fs.writeFile(`finger/${name}.bin`, data, function (err) {
			if (err) return this._log(err);
			this._log(`Write to file ${name}`);
		}.bind(this));
	}

	_request(name,data) {
		var req = request.post(config.api.path.respond_enroll, function (err, resp, body) {
			if (err) {
				this._log('Error!');
			} else {
				this._log('URL: ' + body);
			}
		}.bind(this));
		var form = req.form();
		form.append('file', data , {
			filename: `${name}.bin`,
			contentType: 'application/octet-stream'
		});
		form.append('cmd', 'registerFingerScan');
		form.append('token', this._token);
		form.append('userid', this._userid);
		form.append('fingerType', this._fingerType);

	}

	_log(log) {
		console.log(`Device # ${this.id} > ${log}`);
	}
}
module.exports = SmartIDDevice;