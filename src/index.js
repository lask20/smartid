	let app = require('express')();
	let express = require('express');
	let server = require('http').Server(app);
	let bodyParser = require('body-parser');
	let mqtt = require('mqtt')
	let fs = require('fs');
	let request = require('request');

	let config = require("./config.js");

	let SmartIDDevice = require("./SmartIDDevice");

	let client  = mqtt.connect(config.mqtt.server.host);

	var devices = [];

	app.set('view engine', 'ejs');
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(express.static('public'));

	app.get('/api', function (req, res) {
		request('https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY', { json: true }, (err, resrq, body) => {
			if (err) { return console.log(err); }
			res.json(body);
		});
	});

	app.post('/api/enroll', function (req, res) {
		if (req.body.id != null) {
		var device = findDeviceByID(req.body.id);
		if (device !== 0) {
			device.enroll(req.body.userid,req.body.userfinger,req.body.username,req.body.token);
			res.json({status:"success"});
		}
		else {
			res.json({status:"error",error:"Device not found"});
		}
	}
	res.json({status:"error",error:"Invalid parameter"});
	});

	app.post('/api/synctime', function (req, res) {
		if (req.body.id != null) {
		var device = findDeviceByID(req.body.id);
		if (device !== 0) {
			device.updateTime();
			res.json({status:"success"});
		}
		else {
			res.json({status:"error",error:"Device not found"});
		}
	}
	res.json({status:"error",error:"Invalid parameter"});
	});

	app.post('/api/removefinger', function (req, res) {
		if (req.body.id != null) {
		var device = findDeviceByID(req.body.id);
		if (device !== 0) {
			device.removeFinger(req.body.finger_id);
			res.json({status:"success"});
		}
		else {
			res.json({status:"error",error:"Device not found"});
		}
	}
	res.json({status:"error",error:"Invalid parameter"});
	});

	app.post('/api/removefingerall', function (req, res) {
		if (req.body.id != null) {
		var device = findDeviceByID(req.body.id);
		if (device !== 0) {
			device.removeFingerAll();
			res.json({status:"success"});
		}
		else {
			res.json({status:"error",error:"Device not found"});
		}
	}
	res.json({status:"error",error:"Invalid parameter"});
	});

	app.post('/api/info', function (req, res) {
		if (req.body.id != null) {
		var device = findDeviceByID(req.body.id);
		if (device !== 0) {
			device.updateTime();
			res.json({status:"success",info:{connected:device.isConnected(),id:device.id,model:device.getModel(),version:device.getVersion()}});
		}
		else {
			res.json({status:"error",error:"Device not found"});
		}
	}
	res.json({status:"error",error:"Invalid parameter"});
	});

	function findDeviceByID(id) {
		for (let device of devices) {
			if (device.id === id) {
				return device;
			}
		}
		return 0;
	}

	client.on('connect', function () {
	  // client.subscribe('presence')
	 //  var data = fs.readFileSync('figer_model.ino.bin');
	 console.log("Mqtt connected");
	  // client.publish('fp/1', data)
	  var test = new SmartIDDevice(client);
	  devices.push(test);
	  test.id = "FC14";
	  test.start();
	})

	server.listen(8000);